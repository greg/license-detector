#!/bin/bash

echo $GROUP_ID | tee -a group_ids.txt &&
http --follow GET gitlab.com/api/v4/groups/$GROUP_ID/subgroups Private-Token:$TOKEN | jq . | grep -E '\"id'  | awk '{print $2}' | sed 's/"//g;s/,//g' | tee -a group_ids.txt
