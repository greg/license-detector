#!/bin/bash
# set -x
while read p; do
  license-detector "$p" | tee -a results.txt
done < list.txt
