Enumerate and list projects in group namespace (including subgroup/decendant groups) via API

```bash
#!/bin/bash

# subgroups
http --follow GET gitlab.com/api/v4/groups/$GROUP_ID/subgroups Private-Token:$TOKEN | jq . | grep -E '\"id'
# descendant groups
http --follow GET gitlab.com/api/v4/groups/$GROUP_ID/descendant_groups Private-Token:$TOKEN | jq . | grep -E '\"id'
# 100 group projects 
http --follow GET gitlab.com/api/v4/groups/$GROUP_ID?per_page=100 Private-Token:$TOKEN | grep http_url_to_repo | awk '{print $2}' | sed 's/"//g;s/,//g'
# over 100 group projects
http --follow GET gitlab.com/api/v4/groups/$GROUP_ID?per_page=100&page=$PAGE_NUM Private-Token:$TOKEN | grep http_url_to_repo | awk '{print $2}' | sed 's/"//g;s/,//g'
```
