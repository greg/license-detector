#!/bin/bash

while read g; do
  http --follow GET gitlab.com/api/v4/groups/$GROUP_ID?per_page=100 Private-Token:$TOKEN | jq . |  grep http_url_to_repo | awk '{print $2}' | sed 's/"//g;s/,//g' | tee -a projects.txt
done < group_ids.txt

